package id.ac.stts.qmoticon;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.activities.HomeActivity;
import id.ac.stts.qmoticon.dataObjects.classHighscore;
import id.ac.stts.qmoticon.dataObjects.classUser;

public class fragment_leaderboard extends Fragment {
    RecyclerView rv;
    AdapterLeaderboard adapter;
    ProgressBar pb;

    ArrayList<classHighscore> dataskor;

    Button easy, medium, hard;
    private DocumentSnapshot last;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataskor = new ArrayList<>();
        View v = inflater.inflate(R.layout.fragment_leaderboard_layout, container, false);
        return v;
    }
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv = view.findViewById(R.id.rv_leaderboard);
        pb = (view.findViewById(R.id.pb_Leaderboard));
        easy = view.findViewById(R.id.btn_leaderboard_easy);
        medium = view.findViewById(R.id.btn_leaderboard_med);
        hard = view.findViewById(R.id.btn_leaderboard_hard);

        HomeActivity.getDbInstance().collection("highscore")
                .orderBy("hard", Query.Direction.DESCENDING).limit(50)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                //Toast.makeText(getContext(), "halo", Toast.LENGTH_SHORT).show();
                for(DocumentSnapshot docs: queryDocumentSnapshots.getDocuments()){
                    classHighscore hs = docs.toObject(classHighscore.class);
                    dataskor.add(hs);
                    hs.setUser(docs.getId());
                    System.out.println(hs);
//                    Toast.makeText(getContext(), docs.getId(), Toast.LENGTH_SHORT).show();
                    last = docs;
                }
                adapter = new AdapterLeaderboard(getContext(), dataskor);
                rv.setAdapter(adapter);
                rv.setLayoutManager(new LinearLayoutManager(getContext()));
                adapter.setDiff("easy");
                adapter.notifyDataSetChanged();
                pb.setVisibility(View.GONE);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Snackbar.make(rv, "Gagal", Snackbar.LENGTH_INDEFINITE).show();
            }
        });

        easy . setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "Easy", Toast.LENGTH_SHORT).show();
                adapter.setDiff("easy");
                adapter.notifyDataSetChanged();
            }
        });
        medium . setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "Medium", Toast.LENGTH_SHORT).show();
                adapter.setDiff("medium");
                adapter.notifyDataSetChanged();
            }
        });

        hard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "Hard", Toast.LENGTH_SHORT).show();
                adapter.setDiff("hard");
                adapter.notifyDataSetChanged();
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0){
                    Toast.makeText(getContext(), dy+"", Toast.LENGTH_SHORT).show();
                    HomeActivity.getDbInstance().collection("highscore")
                            .orderBy("hard", Query.Direction.DESCENDING).limit(20).startAfter(last)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    Toast.makeText(getContext(), "halo", Toast.LENGTH_SHORT).show();
                                    for(DocumentSnapshot docs: queryDocumentSnapshots.getDocuments()){
                                        classHighscore hs = docs.toObject(classHighscore.class);
                                        dataskor.add(hs);
                                        hs.setUser(docs.getId());
                                        System.out.println(hs);
                                        Toast.makeText(getContext(), docs.getId(), Toast.LENGTH_SHORT).show();
                                        last = docs;
                                    }
                                    adapter = new AdapterLeaderboard(getContext(), dataskor);
                                    rv.setAdapter(adapter);
                                    rv.setLayoutManager(new LinearLayoutManager(getContext()));
                                    adapter.setDiff("easy");
                                    adapter.notifyDataSetChanged();
                                    pb.setVisibility(View.GONE);

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Snackbar.make(rv, "Gagal", Snackbar.LENGTH_INDEFINITE).show();
                        }
                    });
                }

            }
        });

    }
}
