package id.ac.stts.qmoticon;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.dataObjects.classScore;
import id.ac.stts.qmoticon.roomDatabase.scoreDatabase;
import pl.droidsonroids.gif.GifImageView;

public class fragment_resultgame extends Fragment {
    ProgressBar progressBar;
    Button btnRetry, btnMenu;
    TextView tvUsername, tvScore, tvHighscore;
    //animation
    GifImageView gif;
    FirebaseAuth auth;

    scoreDatabase db;

    int score;
    String difficulty;
    ActivityExtension actParent;
    int hs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_resultgame_layout, container, false);
        btnRetry = v.findViewById(R.id.btn_Result_Retry);
        btnMenu = v.findViewById(R.id.btn_Result_MainMenu);
        tvUsername = v.findViewById(R.id.tvUsername);
        tvScore = v.findViewById(R.id.tvScore);
        tvHighscore = v.findViewById(R.id.tvHighscore);
        gif = v.findViewById(R.id.gifImage);
        gif.setImageResource(R.drawable.fireworks);
        progressBar = v.findViewById(R.id.pb_ResultGame);
        db = Room.databaseBuilder(getContext(), scoreDatabase.class, "scoredb").build();
        auth = FirebaseAuth.getInstance();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        actParent = (ActivityExtension)getActivity();
        progressBar.setVisibility(View.VISIBLE);
        classScore newScore = new classScore(auth.getCurrentUser().getEmail(), difficulty, score);
        tvUsername.setText("Congratulations "+actParent.currentUser.displayName);
        tvScore.setText("Your score just now: "+newScore.score+"");
        tvHighscore.setText("Your high score in this mode: "+this.hs+"");

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actParent.navBar.setVisibility(View.VISIBLE);
                actParent.navBar.setSelectedItemId(R.id.item_play);
                actParent.openFragment(new fragment_menugame());
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actParent.navBar.setVisibility(View.VISIBLE);
                actParent.navBar.setSelectedItemId(R.id.item_leaderboard);
                actParent.openFragment(new fragment_leaderboard());
            }
        });
        new addNewScore().execute(newScore);
    }

    private class addNewScore extends  AsyncTask<classScore, Void, Void>
    {
        @Override
        protected Void doInBackground(classScore... classScores) {
            db.scoreDao().insert(classScores[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            new selectAllScore().execute();
        }
    }


    public void setScore(int score) {
        this.score = score;
    }

    public void setHS(int score) {
        this.hs = score;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    private class selectAllScore extends AsyncTask<Void, Void, List<classScore>> {
        @Override
        protected List<classScore> doInBackground(Void... voids) {
            return db.scoreDao().loadAll();
        }

        @Override
        protected void onPostExecute(List<classScore> classScores) {
            super.onPostExecute(classScores);

        }
    }
}
