package id.ac.stts.qmoticon;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.primitives.Chars;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.apache.commons.text.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.IntStream;

import id.ac.stts.qmoticon.activities.HomeActivity;
import id.ac.stts.qmoticon.dataObjects.classSoal;


/**
 * A simple {@link Fragment} subclass.
 */
public class add_soal extends Fragment {
    private TextView preview;
    private EditText input, inpChoice1, inpChoice2, inpChoice3, inpChoice4;
    private FirebaseFirestore db = HomeActivity.getDbInstance();
    private CollectionReference soal;
    private EditText jawabanBenar;


    private ArrayList<String> emoji = new ArrayList<>();

    public add_soal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        soal = db.collection("questions");
        return inflater.inflate(R.layout.fragment_add_soal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inpChoice1 = view.findViewById(R.id.edt_Admin_inpChoice1);
        inpChoice2 = view.findViewById(R.id.edt_Admin_inpChoice2);
        inpChoice3= view.findViewById(R.id.edt_Admin_inpChoice3);
        inpChoice4 = view.findViewById(R.id.edt_Admin_inpChoice4);
        jawabanBenar = view.findViewById(R.id.edt_Admin_displayJawaban);
        preview = view.findViewById(R.id.tv_Admin_preview);
        input = view.findViewById(R.id.edt_Admin_inpSoal);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    String str = s.toString();

                    emoji.clear();

                    preview.setText(StringEscapeUtils.escapeJava(str));
                    for (int i = 0; i < s.length(); i++) {
                        emoji.add(StringEscapeUtils.escapeJava( String.valueOf(s.charAt(i)) ));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        view.findViewById(R.id.btn_Admin_btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classSoal newSoal = new classSoal();
                newSoal.getListEmoticon().addAll(emoji);
                newSoal.setRawData(preview.getText().toString());
                int count = input.getText().toString().length();
                newSoal.getPilihanJawaban().add(inpChoice1.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(inpChoice2.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(inpChoice3.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(inpChoice4.getText().toString().toLowerCase());

                newSoal.setJawaban(jawabanBenar.getText().toString().toLowerCase());
                newSoal.setPembuat(FirebaseAuth.getInstance().getCurrentUser().getEmail());

                if (count>0 && (adaDiPilihan(newSoal.getPilihanJawaban(), newSoal.getJawaban()))){
                    DocumentReference doc = soal.document();

                    //string mode ini HARUS SAMA PERSIS dengan string difficulty
                    newSoal.setMode((count == 2)?"Easy":(count==4)?"Medium":"Hard");


                    doc.set(newSoal).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getActivity(), "Data inserted!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private boolean adaDiPilihan(ArrayList<String> pilihanJawaban, String jawaban) {
        for (String s: pilihanJawaban) {
            if (s.equals(jawaban))
                return true;
        }
        jawabanBenar.setError("Jawaban tidak terdaftar pada pilihan!");
        Toast.makeText(getActivity(), "Jawaban tidak terdaftar pada pilihan!", Toast.LENGTH_SHORT).show();
        return false;
    }
}
