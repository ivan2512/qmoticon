package id.ac.stts.qmoticon.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import id.ac.stts.qmoticon.AdapterHistory;
import id.ac.stts.qmoticon.AdapterLeaderboard;
import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.dataObjects.classScore;
import id.ac.stts.qmoticon.roomDatabase.scoreDatabase;

public class HistoryActivity extends AppCompatActivity {

    RecyclerView rv;
    AdapterHistory adapter;
    ArrayList<classScore> history;
    scoreDatabase db;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        // init arraylist
        history = new ArrayList<>();

        // get databases
        auth = FirebaseAuth.getInstance();
        db = Room.databaseBuilder(getApplicationContext(), scoreDatabase.class, "scoredb").build();

        // load score dengan email tertentu
        new getAllHistoryTask().execute(auth.getCurrentUser().getEmail());

        rv = findViewById(R.id.rv_history);
        rv.setHasFixedSize(true);
    }


    private class getAllHistoryTask extends AsyncTask<String, Void, Void>
    {
        @Override
        protected Void doInBackground(String... strings) {
            history.clear();
            history.addAll(db.scoreDao().loadScoreByEmail(strings[0]));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            adapter = new AdapterHistory(getApplicationContext(), history);
            rv.setAdapter(adapter);
        }
    }


}
