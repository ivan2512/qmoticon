package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;

import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.dataObjects.classSoal;
import id.ac.stts.qmoticon.view_soal;

public class Edit_Soal extends AppCompatActivity {
    public static String EXTRA_IDX="extra_idx";
    EditText inputsoal,editjawaban,choice1,choice2,choice3,choice4;
    TextView preview;
    Button save;
    String UIDSoal;
    private ArrayList<String> emoji = new ArrayList<>();

    classSoal editing;
    FirebaseFirestore instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__soal);
        instance = HomeActivity.getDbInstance();
        UIDSoal =getIntent().getStringExtra(EXTRA_IDX);
        inputsoal=findViewById(R.id.et_edit_soal);
        choice1=findViewById(R.id.edt_Edit_inpChoice1);
        choice2=findViewById(R.id.edt_Edit_inpChoice2);
        choice3=findViewById(R.id.edt_Edit_inpChoice3);
        choice4=findViewById(R.id.edt_Edit_inpChoice4);
        preview=findViewById(R.id.tv_Edit_preview);
        save=findViewById(R.id.btn_Edit_btnSave);
        editjawaban=findViewById(R.id.edt_Edit_displayJawaban);

        instance.collection("questions").document(UIDSoal).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                editing = documentSnapshot.toObject(classSoal.class);
                editing.setFirebaseUID(UIDSoal);
                editjawaban.setText(editing.getJawaban());
                String temp="";
                emoji=editing.getListEmoticon();
                for (String s:editing.getListEmoticon()) {
                    temp+=s;
                }

                inputsoal.setText(StringEscapeUtils.unescapeJava(temp));
                preview.setText(temp);

                choice1.setText(editing.getPilihanJawaban().get(0));
                choice2.setText(editing.getPilihanJawaban().get(1));
                choice3.setText(editing.getPilihanJawaban().get(2));
                choice4.setText(editing.getPilihanJawaban().get(3));
            }
        });


        inputsoal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s , int i, int i1, int i2) {
                try {
                    String str = s.toString();

                    emoji.clear();

                    preview.setText(StringEscapeUtils.escapeJava(str));
                    for (int k = 0; k < s.length(); k++) {
                        emoji.add(StringEscapeUtils.escapeJava( String.valueOf(s.charAt(k)) ));
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classSoal newSoal = new classSoal();
                newSoal.getListEmoticon().addAll(emoji);
                newSoal.setRawData(preview.getText().toString());
                int count = inputsoal.getText().toString().length();
                newSoal.getPilihanJawaban().add(choice1.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(choice2.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(choice3.getText().toString().toLowerCase());
                newSoal.getPilihanJawaban().add(choice4.getText().toString().toLowerCase());
                newSoal.setPembuat(editing.getPembuat());
                newSoal.setJawaban(editjawaban.getText().toString().toLowerCase());


                if (count>0 && (adaDiPilihan(newSoal.getPilihanJawaban(), newSoal.getJawaban()))){
                    //string mode ini HARUS SAMA PERSIS dengan string difficulty
                    newSoal.setMode((count == 2)?"Easy":(count==4)?"Medium":"Hard");

                    instance.collection("questions").document(UIDSoal).set(newSoal).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(Edit_Soal.this, "Data berhasil di update", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                } else {
                    editjawaban.setError("Jawaban tidak ada pada pilihan jawaban");
                    //Snackbar.make(save, "Answer not found", BaseTransientBottomBar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    private boolean adaDiPilihan(ArrayList<String> pilihanJawaban, String jawaban) {
        for (String s: pilihanJawaban) {
            if (s.equals(jawaban))
                return true;
        }
        editjawaban.setError("Jawaban tidak terdaftar pada pilihan!");
        Toast.makeText(getApplicationContext(), "Jawaban tidak terdaftar pada pilihan!", Toast.LENGTH_SHORT).show();
        return false;
    }

}
