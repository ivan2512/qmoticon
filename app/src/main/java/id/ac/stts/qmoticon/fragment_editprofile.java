package id.ac.stts.qmoticon;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.activities.HomeActivity;
import id.ac.stts.qmoticon.dataObjects.*;


public class fragment_editprofile extends Fragment {
    EditText ednama, edemail, edBio;
    ProgressBar pb;
    classUser currentUser;
    ImageView imgProfile;
    Button btnSaveChanges;

    FirebaseFirestore database = HomeActivity.getDbInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_editprofile_layout, container, false);
        edemail = v.findViewById(R.id.edRegisterEmail);
        ednama = v.findViewById(R.id.edName);
        edBio = v.findViewById(R.id.ed_EditProfile_Bio);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();

        imgProfile = view.findViewById(R.id.img_EditProfile_profilePicture);
        pb = view.findViewById(R.id.pb_EditProfile);
        btnSaveChanges = view.findViewById(R.id.btn_EditProfile_saveChanges);

        if (u != null){
            loadUserData(u);
        }

        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);

                HashMap<String, Object> updateMap = new HashMap<>();
                updateMap.put("email", u.getEmail());
                updateMap.put("uid", u.getUid());
                updateMap.put("displayName", ednama.getText().toString());
                updateMap.put("bio", edBio.getText().toString());

                database.collection("users").document(u.getEmail()).update(updateMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        loadUserData(u);
                    }
                });
            }
        });
    }

    private void loadUserData(final FirebaseUser u) {
        database.collection("users").document(u.getEmail()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                currentUser = documentSnapshot.toObject(classUser.class);
                ednama.setText(currentUser.displayName);
                edemail.setText(documentSnapshot.getId());
                edemail.setInputType(InputType.TYPE_NULL);
                Glide.with(imgProfile).load(u.getPhotoUrl()).override(300,300).into(imgProfile);
                edBio.setText(currentUser.bio);
                pb.setVisibility(View.GONE);

                ((ActivityExtension)getActivity()).currentUser = currentUser;
            }
        });
    }
}
