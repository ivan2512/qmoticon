package id.ac.stts.qmoticon.dataObjects;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import id.ac.stts.qmoticon.R;

public class notificationHelper extends ContextWrapper {
    public static final String channelID = "id";
    public static final String channelName = "name";
    public NotificationManager notificationManager;
    public notificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            createChannels();
        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    private void createChannels() {
        NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        channel.enableLights(true);
        channel.enableVibration(true);
        channel.setLightColor(R.color.colorPrimary);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getNotificationManager().createNotificationChannel(channel);
    }

    public NotificationManager getNotificationManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return notificationManager;
    }
    public NotificationCompat.Builder getNotification() {
        return new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle("We Missing You")
                .setContentText("Let's Play and Beat Your Highscore!")
                .setSmallIcon(R.drawable.logo)
                .setDefaults(Notification.DEFAULT_SOUND);
    }
}
