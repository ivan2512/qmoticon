package id.ac.stts.qmoticon;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Placeholder;
import androidx.fragment.app.Fragment;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.activities.HomeActivity;

public class fragment_menugame extends Fragment {
    Button btnStart, btnPractice;
    ActivityExtension parent;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menugame_layout, container, false);
        btnStart = v.findViewById(R.id.button);
        btnPractice = v.findViewById(R.id.button2);
        setHasOptionsMenu(true);
        registerForContextMenu(btnStart);
        registerForContextMenu(btnPractice);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(view);
            }
        });

        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ActivityExtension)getActivity()).navBar.setVisibility(View.INVISIBLE);
                ((ActivityExtension)getActivity()).play = "practice";
                Fragment fragment = new fragment_practicegame();
                ((ActivityExtension)getActivity()).openFragment(fragment);
            }
        });

        parent = (ActivityExtension) getActivity();
        return v;
    }

    public void showPopup (View view) { //Void Khusus untuk Popup Menu
        PopupMenu popupMenu = new PopupMenu(getContext(), view); //Mendeclare PopupMenu
        popupMenu.inflate(R.menu.menu_difficultygame_context);
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                //string difficulty HARUS SAMA PERSIS dengan string mode di add_soal
                String difficulty = "";
                if(menuItem.getItemId() == R.id.item_easy) difficulty = "Easy";
                else if(menuItem.getItemId() == R.id.item_medium) difficulty = "Medium";
                else if(menuItem.getItemId() == R.id.item_hard) difficulty = "Hard";
                else if(menuItem.getItemId() == R.id.item_mix) difficulty = "mix";
                parent.navBar.setVisibility(View.INVISIBLE);
                parent.play = "play";
                fragment_animation_loading fragment = new fragment_animation_loading();
                fragment.setDifficulty( difficulty );
                parent.difficulty = difficulty;
                parent.openFragment(fragment);

                return true;
            }
        });
    }
}
