package id.ac.stts.qmoticon.dataObjects;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;

public class classSoal {
    private String FirebaseUID;
    private ArrayList<String> listEmoticon;
    private String jawaban;
    private String mode; //easy medium hard
    private String rawData;
    private ArrayList<String> pilihanJawaban;
    private String pembuat;

    public classSoal(){
        listEmoticon=new ArrayList<>();
        pilihanJawaban=new ArrayList<>();
    }

    public classSoal(ArrayList<String> listEmoticon, String jawaban, String mode) {
        this.listEmoticon = listEmoticon;
        this.jawaban = jawaban;
        this.mode = mode;
        pilihanJawaban= new ArrayList<>();
    }

    public ArrayList<String> getListEmoticon() {
        return listEmoticon;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public void setListEmoticon(ArrayList<String> listEmoticon) {
        this.listEmoticon = listEmoticon;
    }

    public ArrayList<String> getPilihanJawaban() {
        return pilihanJawaban;
    }

    public void setPilihanJawaban(ArrayList<String> pilihanJawaban) {
        this.pilihanJawaban = pilihanJawaban;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }

    public String getFirebaseUID() {
        return FirebaseUID;
    }

    public void setFirebaseUID(String firebaseUID) {
        FirebaseUID = firebaseUID;
    }

    public boolean has(String str){
        return this.jawaban.contains(str) || this.rawData.contains(StringEscapeUtils.escapeJava(str));
    }
}
