package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import id.ac.stts.qmoticon.R;

public class RegisterActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseFirestore db;

    RadioGroup rgUserType;
    EditText edDisplayName, edEmail, edBio;
    Button btnRegister;
    ImageView ivPP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Firebase Auth dan Firestore
        auth = FirebaseAuth.getInstance();
        db = HomeActivity.getDbInstance();

        rgUserType = findViewById(R.id.rgRegisterUserType);
        edDisplayName = findViewById(R.id.edRegisterDisplayName);
        edEmail = findViewById(R.id.edRegisterEmail);
        edBio = findViewById(R.id.edRegisterSetBio);
        btnRegister = findViewById(R.id.btnRegister);
        ivPP = findViewById(R.id.ivRegisterPP);

        RadioButton rbPlayer = findViewById(R.id.radioRegisterPlayer);
        rbPlayer.setChecked(true);

        // Load Cridentials
        Glide.with(this)
                .load(auth.getCurrentUser().getPhotoUrl())
                .placeholder(R.color.colorPrimaryDark)
                .into(ivPP);

        edDisplayName.setText(auth.getCurrentUser().getDisplayName());
        edEmail.setText(auth.getCurrentUser().getEmail());

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register()
    {
        String email, bio, tipeuser;
        email = edEmail.getText().toString();
        bio = edBio.getText().toString();
        RadioButton selected = findViewById(rgUserType.getCheckedRadioButtonId());
        tipeuser = selected.getText().toString();

        Map<String, Object> newUser = new HashMap<>();
        newUser.put("displayName", edDisplayName.getText().toString());
        newUser.put("bio", bio);
        newUser.put("type", tipeuser);

        db.collection("users").document(email)
                .set(newUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // sukses finish
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Fail
                        Toast.makeText(RegisterActivity.this, "Register Gagal, Coba Register Lagi", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
