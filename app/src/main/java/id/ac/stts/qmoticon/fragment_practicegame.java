package id.ac.stts.qmoticon;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

import id.ac.stts.qmoticon.activities.ActivityExtension;

public class fragment_practicegame extends Fragment {
    ActivityExtension actParent;
    TextView tvSoal;
    ArrayList<RadioButton> rbAnswers;
    Button btPrev, btNext, btExit;
    ProgressBar pb;

    ArrayList<DocumentSnapshot> soals;
    DocumentSnapshot currentSoal;

    int index = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_practicegame_layout, container, false);
        tvSoal = v.findViewById(R.id.tv_Practice_soal);
        rbAnswers = new ArrayList<>();
        rbAnswers.add((RadioButton)v.findViewById(R.id.radioButton));
        rbAnswers.add((RadioButton)v.findViewById(R.id.radioButton2));
        rbAnswers.add((RadioButton)v.findViewById(R.id.radioButton3));
        rbAnswers.add((RadioButton)v.findViewById(R.id.radioButton4));
        btPrev = v.findViewById(R.id.btn_Practice_prev);
        btNext = v.findViewById(R.id.btn_Practice_next);
        btExit = v.findViewById(R.id.btnExit);
        pb = v.findViewById(R.id.pb_Practioce);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        actParent = (ActivityExtension)getActivity();
        soals = new ArrayList<>();
        ActivityExtension.getDbInstance().collection("questions").limit(5).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    QuerySnapshot qSnapshot = task.getResult();
                    soals.addAll(qSnapshot.getDocuments());
                    index = 0;
                    displayCurrentSoal();
                    pb.setVisibility(View.GONE);
                }
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                btPrev.setEnabled(true);
                if (index >= soals.size()){
                    pb.setVisibility(View.VISIBLE);
                    ActivityExtension.getDbInstance().collection("questions").startAfter(currentSoal).limit(5).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            pb.setVisibility(View.GONE);
                            if (queryDocumentSnapshots.isEmpty()){
                                btNext.setEnabled(false);
                                index--;
                            } else{
                                soals.addAll(queryDocumentSnapshots.getDocuments());
                                displayCurrentSoal();
                            }
                        }
                    });
                } else {
                    displayCurrentSoal();
                }
            }
        });

        btPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                btNext.setEnabled(true);
                displayCurrentSoal();
            }
        });

        btExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getContext(), v);
                popupMenu.inflate(R.menu.confirm_menu);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()){
                            case R.id.menu_yes:
                                actParent.navBar.setVisibility(View.VISIBLE);
                                actParent.openFragment(new fragment_menugame());
                                return true;
                        }
                        return false;
                    }
                });
            }
        });


    }

    private void displayCurrentSoal() {
        if (index >= 0 && index < soals.size()){
            DocumentSnapshot satu = soals.get(index);
            tvSoal.setText(StringEscapeUtils.unescapeJava(satu.getString("rawData")));
            List<String> jawaban = (List<String>) satu.get("pilihanJawaban");
            for (int i = 0; i < rbAnswers.size(); i++) {
                RadioButton elem = rbAnswers.get(i);
                elem.setText(jawaban.get(i));
                if (jawaban.get(i).equalsIgnoreCase(satu.getString("jawaban"))){
                    elem.setBackgroundColor(Color.GREEN);
                } else {
                    elem.setBackground(tvSoal.getBackground());
                }
            }
            currentSoal = satu;
        }
        else {
            index++;
            btPrev.setEnabled(false);
        }
    }
}
