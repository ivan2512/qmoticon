package id.ac.stts.qmoticon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import id.ac.stts.qmoticon.dataObjects.classScore;

public class AdapterHistory extends RecyclerView.Adapter<AdapterHistory.HistoryViewHolder> {
    Context context;
    ArrayList<classScore> history;

    public AdapterHistory(Context context, ArrayList<classScore> history) {
        this.context = context;
        this.history = history;
    }

    @NonNull
    @Override
    public AdapterHistory.HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_history, parent, false);
        return new AdapterHistory.HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHistory.HistoryViewHolder holder, int position) {
        classScore sc = history.get(position);
        holder.tvScore.setText(sc.score + "");
        holder.tvDiff.setText(sc.diff);
        holder.tvCounter.setText((position+1)+"");
    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCounter, tvDiff, tvScore;

        public HistoryViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            tvDiff = itemView.findViewById(R.id.tvDiff);
            tvScore = itemView.findViewById(R.id.tvScore);
        }
    }
}
