package id.ac.stts.qmoticon.roomDatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import id.ac.stts.qmoticon.dataObjects.classScore;

@Database(entities = {classScore.class}, version = 1, exportSchema = false)
public abstract class scoreDatabase extends RoomDatabase
{
    public abstract  scoreDAO scoreDao();
}
