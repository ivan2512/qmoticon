package id.ac.stts.qmoticon.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.dataObjects.classUser;

public class ActivityExtension extends AppCompatActivity {
    private static FirebaseFirestore db = FirebaseFirestore.getInstance();
    public static FirebaseFirestore getDbInstance(){
        if (db==null) {
            db = FirebaseFirestore.getInstance();
        }
        return db;
    }
    public String score = ""; //untuk result
    public String difficulty = ""; //untuk tingkat kesulitan
    public String play = ""; //untuk mode play atau tutorial (retry)
    public BottomNavigationView navBar;
    public classUser currentUser;

    public void openFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.commit();
    }
}
