package id.ac.stts.qmoticon.roomDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import id.ac.stts.qmoticon.dataObjects.classScore;

@Dao
public interface scoreDAO {

    @Query("SELECT * FROM CLASSSCORE order by score desc")
    List<classScore> loadAll();

    @Query("SELECT * FROM CLASSSCORE WHERE email = :email ORDER BY id DESC")
    List<classScore> loadScoreByEmail(String email);

    @Insert
    void insert(classScore... score);

    @Delete
    void delete(classScore score);

}
