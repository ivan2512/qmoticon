package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.add_soal;
import id.ac.stts.qmoticon.dataObjects.classUser;
import id.ac.stts.qmoticon.fragment_editprofile;
import id.ac.stts.qmoticon.fragment_leaderboard;
import id.ac.stts.qmoticon.fragment_menugame;
import id.ac.stts.qmoticon.view_soal;

public class AdminActivity extends ActivityExtension {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        try {
            String userString = getIntent().getStringExtra(MainActivity.CURRENT_USER);
            System.out.println("userstring" + userString);
            JSONObject obj = new JSONObject(userString);

            currentUser = new classUser();
            currentUser.type = obj.getString("type");
            currentUser.displayName = obj.getString("displayName");
            currentUser.email = obj.getString("email");
            currentUser.bio = obj.getString("bio");
            //Snackbar.make(findViewById(R.id.bottomnavbar_Admin), userString, Snackbar.LENGTH_INDEFINITE).show();
        } catch (Exception e){
            e.printStackTrace();
            Snackbar.make(findViewById(R.id.bottomnavbar_Admin), e.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
        }
        System.out.println(currentUser);
        openFragment(new add_soal());
        navBar = findViewById(R.id.bottomnavbar_Admin);
        navBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.item_add_soal:
                        fragment = new add_soal();
                        break;
                    case R.id.item_view_soal:
                        fragment = new view_soal();
                        break;
                    case R.id.item_play:
                        fragment = new fragment_menugame();
                        break;
                    case R.id.item_leaderboard:
                        fragment = new fragment_leaderboard();
                        break;
                    case R.id.item_editprofile:
                        fragment = new fragment_editprofile();
                        break;
                }
                openFragment(fragment);
                return true;
            }
        });
    }

    @Override
    public void openFragment(Fragment fragment){
        if (fragment != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_Admin, fragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optmenulogout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuLogout:
                Toast.makeText(this, "Logout Creator", Toast.LENGTH_LONG).show();
                AuthUI.getInstance().signOut(this).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(AdminActivity.this, MainActivity.class));
                        finish();
                    }
                });
                return true;
            case R.id.menuHistory:
                Intent intent = new Intent(AdminActivity.this, HistoryActivity.class);
                startActivity(intent);

                return true;
        }
        return false;
    }
}
