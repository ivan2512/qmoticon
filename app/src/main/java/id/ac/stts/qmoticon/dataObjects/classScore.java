package id.ac.stts.qmoticon.dataObjects;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class classScore {
    // class untuk simpan room
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "email")
    @NonNull
    public String email;

    @ColumnInfo(name = "difficulty")
    public String diff;

    @ColumnInfo(name = "score")
    public int score;

    public classScore(String email, String diff, int score) {
        this.email = email;
        this.diff = diff;
        this.score = score;
    }

}
