package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.dataObjects.classUser;
import id.ac.stts.qmoticon.fragment_editprofile;
import id.ac.stts.qmoticon.fragment_leaderboard;
import id.ac.stts.qmoticon.fragment_menugame;

public class HomeActivity extends ActivityExtension{
    Fragment fragment = new fragment_menugame();
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        progressBar = findViewById(R.id.pb_Home);
        progressBar.setVisibility(View.VISIBLE);
        navBar = findViewById(R.id.bottomnavbar);
        navBar.setOnNavigationItemSelectedListener(bottomnavlistener);
        try {
            String userString = getIntent().getStringExtra(MainActivity.CURRENT_USER);
            System.out.println("userstring" + userString);
            JSONObject obj = new JSONObject(userString);

            currentUser = new classUser();
            currentUser.type = obj.getString("type");
            currentUser.displayName = obj.getString("displayName");
            currentUser.email = obj.getString("email");
            currentUser.bio = obj.getString("bio");
            progressBar.setVisibility(View.GONE);
        } catch (Exception e){
            e.printStackTrace();
            Snackbar.make(progressBar, e.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
        }
        navBar.setSelectedItemId(R.id.item_play);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomnavlistener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.item_play:
                    fragment = new fragment_menugame();
                    break;
                case R.id.item_leaderboard:
                    fragment = new fragment_leaderboard();
                    break;
                case R.id.item_editprofile:
                    fragment = new fragment_editprofile();
                    break;
            }
            openFragment(fragment);
            return true;
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optmenulogout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuLogout:
                Toast.makeText(this, "Logout Player", Toast.LENGTH_LONG).show();
                AuthUI.getInstance().signOut(this).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(HomeActivity.this, MainActivity.class));
                        finish();
                    }
                });
                return true;
            case R.id.menuHistory:
                Intent intent = new Intent(HomeActivity.this, HistoryActivity.class);
                startActivity(intent);

                return true;
        }
        return false;
    }



}
