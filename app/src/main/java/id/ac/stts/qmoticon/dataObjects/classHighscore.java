package id.ac.stts.qmoticon.dataObjects;

import androidx.annotation.NonNull;

public class classHighscore {
    // class untuk integrasi dengan firebase (Highscore)
    public int easy, medium, hard;
    public String useremail;

    public String usernama;

    public classHighscore() {
        this.easy = 0;
        this.medium = 0;
        this.hard = 0;
    }

    public classHighscore(int easy, int medium, int hard) {
        this.easy = easy;
        this.medium = medium;
        this.hard = hard;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("Easy: %d, Medium: %d, Hard: %d", easy, medium, hard);
    }

    public void setUser(String user) {
        this.useremail = user;
    }

    public String getUser() {
        return useremail;
    }

    public String getUsernama() {
        return usernama;
    }

    public void setUsernama(String usernama) {
        this.usernama = usernama;
    }

    public int getEasy() {
        return easy;
    }

    public void setEasy(int easy) {
        this.easy = easy;
    }

    public int getHard() {
        return hard;
    }

    public void setHard(int hard) {
        this.hard = hard;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }
}
