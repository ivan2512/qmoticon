package id.ac.stts.qmoticon;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import pl.droidsonroids.gif.GifImageView;
import id.ac.stts.qmoticon.activities.*;
import id.ac.stts.qmoticon.dataObjects.*;

public class fragment_game extends Fragment {
    int pStatus = 60; //untuk progressbar
    TextView tvPG;
    ProgressBar pgbar;

    TextView tvSoal, tvScore; //untuk widget soal
    RadioGroup rbGroup;
    RadioButton rbA, rbB, rbC, rbD;

    String jawaban; //variabel temp
    int score = 0;

    //animation
    GifImageView gif;
    Random rand = new Random();

    ActivityExtension actParent;
    FirebaseFirestore database = HomeActivity.getDbInstance();
    FirebaseAuth auth = FirebaseAuth.getInstance();
    QuerySnapshot soal;
    int counter;

    ArrayList<RadioButton> answersButton;
    ArrayList<classSoal> listSoalLengkap;
    classSoal currentQuestion;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_game_layout, container, false);
        actParent = (ActivityExtension) getActivity();
        pgbar = v.findViewById(R.id.progressBar);
        tvPG = v.findViewById(R.id.textView);
        gif = v.findViewById(R.id.gifImage);

        tvSoal = v.findViewById(R.id.tvSoal);
        tvScore= v.findViewById(R.id.tvScore);
        rbGroup = v.findViewById(R.id.rbGroup);
        answersButton = new ArrayList<>();
        rbA = v.findViewById(R.id.radioButton);
        rbB = v.findViewById(R.id.radioButton2);
        rbC = v.findViewById(R.id.radioButton3);
        rbD = v.findViewById(R.id.radioButton4);
        answersButton.add(rbA);
        answersButton.add(rbB);
        answersButton.add(rbC);
        answersButton.add(rbD);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton rb = (RadioButton)v;
                if (rb.getText().toString().equalsIgnoreCase(currentQuestion.getJawaban())){
                    rb.setBackgroundColor(Color.GREEN);
                    score ++;
                    tvScore.setText(score + "");
                } else {
                    rb.setBackgroundColor(Color.RED);
                }
                counter++;

                new CountDownTimer(100,100) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        setRadioToNormal();
                        displayCurrentQuestion();
                    }
                }.start();

            }
        };
        for (RadioButton rb:answersButton) {
            rb.setOnClickListener(listener);
        }
        gif.setVisibility(View.VISIBLE);
        disableBoardSoal();
        //getAllSoal();

        gif.setImageResource(R.drawable.countdown);
        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {}
            public void onFinish() {
                gif.setVisibility(View.INVISIBLE);
                setBoardSoal();
                new CountDownTimer(61000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        tvPG.setText(""+pStatus);
                        int temp  = Integer.parseInt(millisUntilFinished+"");
                        pgbar.setProgress(60-pStatus);
                        pStatus--;
                    }

                    public void onFinish() {
                        final int scoretemp = score;
                        disableBoardSoal();

                        // INSERT HASIL GAME KE FIRESTORE
                        // get document
                        DocumentReference doc = database.collection("highscore").document(auth.getCurrentUser().getEmail());
                        doc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
                        {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task)
                            {

                                DocumentSnapshot document = task.getResult();
                                classHighscore temp;
                                if(document.exists())
                                {
                                    // document exist
                                    temp = document.toObject(classHighscore.class);
                                    System.out.println(temp.toString());
                                }
                                else
                                {
                                    temp = new classHighscore();
                                }

                                fragment_resultgame fragment = new fragment_resultgame();
                                fragment.setDifficulty(actParent.difficulty);
                                fragment.setScore(scoretemp);

                                if(actParent.difficulty.equalsIgnoreCase("Easy"))
                                {
                                    if(scoretemp > temp.easy)
                                    {
                                        temp.easy = scoretemp;
                                    }
                                    fragment.setHS(temp.easy);
                                }
                                else if(actParent.difficulty.equalsIgnoreCase("Medium"))
                                {
                                    if(scoretemp > temp.medium)
                                    {
                                        temp.medium = scoretemp;
                                    }
                                    fragment.setHS(temp.medium);

                                }
                                else if(actParent.difficulty.equalsIgnoreCase("Hard"))
                                {
                                    if(scoretemp > temp.hard)
                                    {
                                        temp.hard = scoretemp;
                                    }
                                    fragment.setHS(temp.hard);

                                }

                                Map<String, Object> newHighscore = new HashMap<>();
                                newHighscore.put("useremail",actParent.currentUser.email);
                                newHighscore.put("usernama",actParent.currentUser.displayName);
                                newHighscore.put("easy", temp.easy);
                                newHighscore.put("medium", temp.medium);
                                newHighscore.put("hard", temp.hard);
                                database.collection("highscore").document(auth.getCurrentUser().getEmail()).set(newHighscore);

                                actParent.openFragment(fragment);

                                //gif.setVisibility(View.INVISIBLE);

                                pgbar.setProgress(60); tvPG.setText("0");
                                disableBoardSoal();
                            }
                        });
                    }
                }.start();
            }
        }.start();

        return v;
    }
    public void setSoal(QuerySnapshot q){
        soal = q;
        listSoalLengkap = new ArrayList<>();
        for (DocumentSnapshot doc: soal.getDocuments()) {
            listSoalLengkap.add( doc.toObject(classSoal.class) );
        }
        counter = 0;
    }

    private void setBoardSoal(){
        tvSoal.setVisibility(View.VISIBLE);
        tvScore.setVisibility(View.VISIBLE);
        rbGroup.setVisibility(View.VISIBLE);
        pgbar.setVisibility(View.VISIBLE);
        tvPG.setVisibility(View.VISIBLE);
        pStatus = 60;
        displayCurrentQuestion();
    }

    private void displayCurrentQuestion() {
        rand = new Random();
        counter = rand.nextInt(listSoalLengkap.size());
        //counter %= listSoalLengkap.size();

        currentQuestion = listSoalLengkap.get(counter);
        setRandomJawaban();
        tvSoal.setText(StringEscapeUtils.unescapeJava(currentQuestion.getRawData()));
        for (int i = 0; i < currentQuestion.getPilihanJawaban().size(); i++) {
            answersButton.get(i).setText(currentQuestion.getPilihanJawaban().get(i));
            answersButton.get(i).setChecked(false);
        }
    }
    public void setRandomJawaban(){
        ArrayList<String> temp = currentQuestion.getPilihanJawaban();
        for(int i=0; i<5; i++){
            rand = new Random();
            int temp1 = rand.nextInt(4);
            int temp2 = rand.nextInt(4);
            String tempString = temp.get(temp1);
            temp.set(temp1, temp.get(temp2));
            temp.set(temp2, tempString);
        }
        currentQuestion.setPilihanJawaban(temp);
    }

    private void disableBoardSoal(){
        tvSoal.setVisibility(View.INVISIBLE);
        tvScore.setVisibility(View.INVISIBLE);
        rbGroup.setVisibility(View.INVISIBLE);
        pgbar.setVisibility(View.INVISIBLE);
        tvPG.setVisibility(View.INVISIBLE);
        score = 0;
        tvScore.setText("Score : 0");
    }

    private void setRadioToNormal(){
        for (RadioButton rb:answersButton) {
            rb.setBackground(tvSoal.getBackground());
            rb.setChecked(false);
        }
    }
}
