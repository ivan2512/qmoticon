package id.ac.stts.qmoticon;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import org.apache.commons.text.StringEscapeUtils;

import java.util.List;

import id.ac.stts.qmoticon.activities.Edit_Soal;
import id.ac.stts.qmoticon.dataObjects.classSoal;

public class AdapterMasterSoal extends RecyclerView.Adapter<AdapterMasterSoal.ViewHolder> {

    List<classSoal> loadedQuestions;
    Context context;
    public AdapterMasterSoal(List<classSoal> loadedQuestions, Context ctx) {
        this.loadedQuestions = loadedQuestions;
        this.context=ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_master_soal, parent, false);
        return new AdapterMasterSoal.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMasterSoal.ViewHolder holder, final int position) {
        holder.bind(loadedQuestions.get(position));
    }

    @Override
    public int getItemCount() {
        return loadedQuestions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSoal, tvPenulis, tvJawaban;
        private Button btnEdit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSoal = itemView.findViewById(R.id.tv_item_soal);
            tvPenulis = itemView.findViewById(R.id.tv_item_penulis);
            tvJawaban = itemView.findViewById(R.id.tv_item_jawaban);
            btnEdit = itemView.findViewById(R.id.btn_item_soal);
        }

        void bind(final classSoal soal){
            tvSoal.setText(StringEscapeUtils.unescapeJava(soal.getRawData()));
            tvPenulis.setText(soal.getPembuat());
            tvJawaban.setText(soal.getJawaban());
            if (soal.getPembuat().equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getEmail())){
                btnEdit.setEnabled(true);
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, Edit_Soal.class);
                        i.putExtra(Edit_Soal.EXTRA_IDX, soal.getFirebaseUID());
                        //Toast.makeText(context, position+"", Toast.LENGTH_SHORT).show();
                        context.startActivity(i);
                    }
                });
            } else {
                btnEdit.setEnabled(false);
            }
        }
    }
}
