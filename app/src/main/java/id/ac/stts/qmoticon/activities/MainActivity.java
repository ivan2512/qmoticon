package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Pair;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import id.ac.stts.qmoticon.R;

public class MainActivity extends AppCompatActivity {
    public static final String CURRENT_USER = "currentuser";
    public static final int RC_LOGIN = 101;
    public static final int RC_HOME = 101;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    AuthUI authUI = AuthUI.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MainActivity ownref = this;

        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    ownref.sendToDashboard(user);
                }
            }
        });

        findViewById(R.id.btnLogin).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final List<AuthUI.IdpConfig> providers = Arrays.asList(
                                new AuthUI.IdpConfig.EmailBuilder().build(),
                                new AuthUI.IdpConfig.GoogleBuilder().build());
                        startActivityForResult(
                                authUI.createSignInIntentBuilder()
                                        .setAvailableProviders(providers)
                                        .build()
                                , MainActivity.RC_LOGIN);
                    }
                }
        );
//        System.out.println(ActivityExtension.ref.getCurrentUser().getEmail());

    }

    private void sendToDashboard(FirebaseUser user) {
        ActivityExtension.getDbInstance().collection("users").document(user.getEmail()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    if (documentSnapshot.get("bio")!= null && documentSnapshot.get("displayName") != null && documentSnapshot.get("type") != null){
                        Intent intent;
                        if (documentSnapshot.getString("type").equalsIgnoreCase("creator")){
                            intent = new Intent(MainActivity.this, AdminActivity.class);
                        }
                        else {
                            intent = new Intent(MainActivity.this, HomeActivity.class);
                        }

                        JSONObject obj = new JSONObject(documentSnapshot.getData());
                        try {
                            obj.put("email", documentSnapshot.getId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        intent.putExtra(CURRENT_USER, obj.toString());
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainActivity.RC_LOGIN && resultCode == RESULT_OK){
            startActivity( new Intent(MainActivity.this, HomeActivity.class));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
