package id.ac.stts.qmoticon;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import id.ac.stts.qmoticon.activities.HomeActivity;
import id.ac.stts.qmoticon.dataObjects.classSoal;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class view_soal extends Fragment {
    RecyclerView rv;
    AdapterMasterSoal adapter;
    FirebaseFirestore database;
    ProgressBar pbLoadSoal;
    ArrayList<classSoal> display;
    ArrayList<classSoal> temp;
    public view_soal() {
        database = HomeActivity.getDbInstance();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_soal, container, false);
    }

    Button btSearch, btFilter;
    EditText edSearch;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbLoadSoal = view.findViewById(R.id.pbar_view);
        rv = view.findViewById(R.id.rv_view_soal);
        temp= new ArrayList<>();
        display = new ArrayList<>();
        btSearch = view.findViewById(R.id.btn_View_Search);
        btFilter = view.findViewById(R.id.btn_View_Filter);
        edSearch = view.findViewById(R.id.edt_View_Search);

        btFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getContext(), v); //Mendeclare PopupMenu
                popupMenu.inflate(R.menu.menu_difficultygame_context);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.item_easy:
                                display.clear();
                                for (classSoal q:temp) {
                                    if (q.getMode().equalsIgnoreCase("easy"))
                                        display.add(q);
                                }
                                adapter.notifyDataSetChanged();
                                break;
                            case R.id.item_medium:
                                display.clear();
                                for (classSoal q:temp) {
                                    if (q.getMode().equalsIgnoreCase("medium"))
                                        display.add(q);
                                }
                                adapter.notifyDataSetChanged();
                                break;
                            case R.id.item_hard:
                                display.clear();
                                for (classSoal q:temp) {
                                    if (q.getMode().equalsIgnoreCase("hard"))
                                        display.add(q);
                                }
                                adapter.notifyDataSetChanged();
                                break;
                            default:
                                    display.clear();
                                    display.addAll(temp);
                        }
                        return true;
                    }
                });
            }
        });

        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = edSearch.getText().toString();
                if (s.length() > 0){
                    for (int i = display.size()-1; i >-1 ; i--) {
                        System.out.println(display.get(i).getRawData() + " - " + s);
                        if (!display.get(i).has(s)){
                            display.remove(i);
                        }
                    }
                } else {
                    display.clear();
                    display.addAll(temp);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    void loadData(){
        display.clear();
        temp.clear();
        pbLoadSoal.setVisibility(View.VISIBLE);
        database.collection("questions").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot snap:queryDocumentSnapshots ) {
                    classSoal question = snap.toObject(classSoal.class);
                    question.setFirebaseUID(snap.getId());
                    temp.add(question);
                    display.add(question);
                }
                pbLoadSoal.setVisibility(View.GONE);
                adapter = new AdapterMasterSoal(display,getContext());
                rv.setAdapter(adapter);
                rv.setLayoutManager(new LinearLayoutManager(getContext()));
                rv.setHasFixedSize(false);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Snackbar.make(pbLoadSoal, "Get data gagal", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
}
