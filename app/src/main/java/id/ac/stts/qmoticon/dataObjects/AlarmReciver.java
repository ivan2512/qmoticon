package id.ac.stts.qmoticon.dataObjects;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

public class AlarmReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        notificationHelper notificationHelper = new notificationHelper(context);
        NotificationCompat.Builder builder = notificationHelper.getNotification();
        notificationHelper.getNotificationManager().notify(1, builder.build());
    }

}
