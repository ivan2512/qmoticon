package id.ac.stts.qmoticon;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.activities.HomeActivity;
import id.ac.stts.qmoticon.dataObjects.classSoal;
import pl.droidsonroids.gif.GifImageView;

public class fragment_animation_loading extends Fragment {
    GifImageView gif;
    FirebaseFirestore database = HomeActivity.getDbInstance();

    private String diff;
    private QuerySnapshot listSoal;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_animationloading_layout, container, false);
        gif = v.findViewById(R.id.gifImage);

        gif.setImageResource(R.drawable.loading);

//        new CountDownTimer(1000, 1000) {
//
//            public void onTick(long millisUntilFinished) {}
//            public void onFinish() {
//
//            }
//        }.start();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (diff.equalsIgnoreCase("mix")){
            database.collection("questions").limit(60).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    listSoal = queryDocumentSnapshots;
                    fragment_game fragment = new fragment_game();
                    fragment.setSoal(listSoal);
                    ((ActivityExtension)getActivity()).openFragment(fragment);
                }
            });
        }
        else {
            database.collection("questions").whereEqualTo("mode", diff).limit(60).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    listSoal = queryDocumentSnapshots;
                    fragment_game fragment = new fragment_game();
                    fragment.setSoal(listSoal);
                    ((ActivityExtension)getActivity()).openFragment(fragment);
                }
            });
        }

    }

    public void setDifficulty(String str){
        diff = str;
    }
}
