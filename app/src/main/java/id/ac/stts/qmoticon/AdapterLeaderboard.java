package id.ac.stts.qmoticon;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import id.ac.stts.qmoticon.activities.ActivityExtension;
import id.ac.stts.qmoticon.dataObjects.classHighscore;
import id.ac.stts.qmoticon.dataObjects.classUser;

public class AdapterLeaderboard extends RecyclerView.Adapter<AdapterLeaderboard.leaderboardViewHolder> {

    Context context;
    List<classHighscore> scores;
    int diff;

    public AdapterLeaderboard(Context context, ArrayList<classHighscore> dataskor) {
        this.context = context;
        this.scores = dataskor;
        diff = 0;
    }

    void setDiff(String diff){
        if (diff.equals("easy")){
            this.diff = 0;
            for (int i = 0; i < scores.size(); i++) {
                for (int j = i; j < scores.size(); j++) {
                    if (scores.get(i).easy < scores.get(j).easy){
                        classHighscore t = scores.get(i);
                        scores.set(i, scores.get(j));
                        scores.set(j, t);
                    }
                }
            }
        }
        else if (diff.equals("medium")){
            this.diff = 1;
            for (int i = 0; i < scores.size(); i++) {
                for (int j = i; j < scores.size(); j++) {
                    if (scores.get(i).medium < scores.get(j).medium){
                        classHighscore t = scores.get(i);
                        scores.set(i, scores.get(j));
                        scores.set(j, t);
                    }
                }
            }
        }
        else if (diff.equals("hard")){
            this.diff = 2;
            for (int i = 0; i < scores.size(); i++) {
                for (int j = i; j < scores.size(); j++) {
                    if (scores.get(i).hard < scores.get(j).hard){
                        classHighscore t = scores.get(i);
                        scores.set(i, scores.get(j));
                        scores.set(j, t);
                    }
                }
            }
        }
    }

    @NonNull
    @Override
    public AdapterLeaderboard.leaderboardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_leaderboard, parent, false);
        return new leaderboardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterLeaderboard.leaderboardViewHolder holder, int position) {
        classHighscore currentScore = scores.get(position);

        holder.tvUsername.setText(currentScore.getUsernama());
        switch (diff){
            case 0:
                holder.tvScore.setText("Score : " + currentScore.easy);
                break;
            case 1:
                holder.tvScore.setText("Score : " + currentScore.medium);
                break;
            case 2:
                holder.tvScore.setText("Score : " + currentScore.hard);
                break;
        }

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(20);
        shape.setColor(Color.argb(100,0,0,0));
        shape.setStroke(5, Color.BLACK);
        holder.tvLogo.setBackground(shape);
        holder.tvLogo.setText((position+1)+"");
        holder.imgdots.setVisibility(View.INVISIBLE);
        holder.tvLogotiny.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    public class leaderboardViewHolder extends RecyclerView.ViewHolder {
        TextView tvLogo, tvUsername, tvScore;
        TextView tvLogotiny;
        ConstraintLayout cl;
        ImageView imgdots;
        public leaderboardViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLogo = itemView.findViewById(R.id.textViewLogo);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            tvScore = itemView.findViewById(R.id.tvScore);
            cl = itemView.findViewById(R.id.cl_container);

            imgdots = itemView.findViewById(R.id.imgDots);
            tvLogotiny = itemView.findViewById(R.id.tvLogoTiny);
        }
    }
}
