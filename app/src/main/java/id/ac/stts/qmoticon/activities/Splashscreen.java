package id.ac.stts.qmoticon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;
import java.util.Date;

import id.ac.stts.qmoticon.R;
import id.ac.stts.qmoticon.dataObjects.AlarmReciver;


public class Splashscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

//        ActivityExtension.ref.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                if (firebaseAuth.getCurrentUser() != null){
//                    Toast.makeText(Splashscreen.this, "User is IN", Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    Toast.makeText(Splashscreen.this, "User is OUT", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        // hide title bar
        getSupportActionBar().hide();




        //set notif
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY,3);
       // Toast.makeText(this, calendar.getTime().toString(), Toast.LENGTH_LONG).show();
        startAlarm(calendar);

        // Set GIF ke Container
        ImageView gifContainer = findViewById(R.id.gifContainer);
        Glide.with(this).load(R.drawable.emoji).transition(withCrossFade()).into(gifContainer);

        //start activity delay 1500 millis
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(Splashscreen.this, MainActivity.class));
                finish();
            }
        }, 1500);


    }

    private void startAlarm(Calendar calendar) {

            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), AlarmReciver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

    }


    private void cancelAlarm() {
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlarmReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        alarmManager.cancel(pendingIntent);
    }
}
